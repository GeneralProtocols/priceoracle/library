// Import the debug logging facility.
import { debug } from './logging';

// Import protocol data functions, constants and interfaces
import { OracleData } from './data';
import { OracleProtocol } from './protocol';
import type { IncomingPacketCallback, IncomingRequestCallback } from './interfaces';

// Import functions to support encoding and decoding array buffers with JSON.
import { replacer, reviver } from 'json-arraybuffer-reviver';

// Import library for managing hex strings.
import { binToHex, hexToBin } from '@bitauth/libauth';

// Add support for streaming network messages.
import { Subscriber, Publisher, Request, Reply } from 'zeromq';

// Add support for handling concurrency.
import { Mutex } from 'async-mutex';

// Set up concurrency locks for zmq socket send (enqueue-ing) / receive (dequeue-ing)
const sendLock = new Mutex();
const requestLock = new Mutex();

/**
 * Oracle Network Management Class
 */
export class OracleNetwork
{
	/**
	 * Counter for number of broadcast packets received.
	 */
	static broadcastedPacketCounter: number;

	/**
	 * List of SocketUrl to boolean to indicate whether to listen for broadcasts on a given SocketURL.
	 * @private
	 */
	static shouldReconnectWithBroadcast: Record<string, boolean>;

	/**
	 * List of Broadcasting Sockets we are listening to (as SocketUrl to ZeroMQ Subscriber).
	 * @private
	 */
	static broadcastSubscriber: Record<string, Subscriber>;

	/**
	 * ZeroMQ Publisher Socket for broadcasting packets.
	 * @private
	 */
	static broadcastPublisher: Publisher | undefined;

	/**
	 * List of Relay Sockets we will relay packets to (as SocketUrl to ZeroMQ Publisher).
	 * @private
	 */
	static relayPublishers: Record<string, Publisher>;

	/**
	 * Counter for number of relay packets received.
	 */
	static relayedPacketCounter: number;

	/**
	 * ZeroMQ Subscriber Socket for listening to relays.
	 * @private
	 */
	static relaySubscriber: Subscriber | undefined;

	/**
	 * Boolean to specify whether we should continuously listen to relays.
	 * @private
	 */
	static shouldReconnectWithRelays: boolean;

	/**
	 * Counter for number of request packets received.
	 */
	static requestPacketCounter: number;

	/**
	 * List of SocketUrl to ZeroMQ for listening to Requests.
	 * @private
	 */
	static requestSockets: Record<string, Request>;

	/**
	 * Boolean to specify whether we should listen for requests.
	 * @private
	 */
	static shouldReconnectWithRequests: boolean;

	/**
	 * Reply Socket for ZeroMQ.
	 * @private
	 */
	static replySocket: Reply | undefined;

	/**
	 * Getter used to craft consistent socket urls.
	 *
	 * @param address   {string}     a hostname or IP address.
	 * @param port      {number}     a port number.
	 *
	 * @returns {string} a socket url in the form of tcp://address:port
	 */
	static getSocketUrl(address: string, port: number): string
	{
		// Craft a socket URL from the parameters provided.
		return `tcp://${address}:${port}`;
	}

	/**
	 * Handler for incoming packets.
	 *
	 * @param callback  {function}   a function that will handle incoming packets, taking topic and content as function parameters.
	 * @param address   {string}     a hostname or IP address which the broadcast socket will bind to.
	 * @param port      {number}     a port number which the broadcast socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async receiveBroadcastPackets(callback: IncomingPacketCallback, address: string, port: number): Promise<void>
	{
		// Craft the socket url.
		const socketUrl = this.getSocketUrl(address, port);

		// Initialize a packet counter.
		this.broadcastedPacketCounter = 0;

		// Initialize list of broadcast reconnection status, if necessary.
		if(!this.shouldReconnectWithBroadcast)
		{
			this.shouldReconnectWithBroadcast = {};
		}

		// Initialize a boolean controlling when to stop listening to broadcasts for this socketUrl.
		this.shouldReconnectWithBroadcast[socketUrl] = true;

		// Continuously reconnect and listen to relay until toggled off.
		while(this.shouldReconnectWithBroadcast[socketUrl])
		{
			// Create a subscriber socket.
			this.broadcastSubscriber[socketUrl] = new Subscriber();

			// Start accepting connections from peers who want their price messages relayed.
			await this.broadcastSubscriber[socketUrl].connect(socketUrl);

			// Subscribe to all messages.
			await this.broadcastSubscriber[socketUrl].subscribe();

			// Log that we are now listening for broadcasted messages.
			debug.action(`Listening for broadcasted messages on ${address}:${port}.`);

			// Wrap in try-catch to ensure that errors does not interrupt service.
			try
			{
				// Iterate over incoming packets until the connection is broken.
				for await (const [ topic, content ] of this.broadcastSubscriber[socketUrl])
				{
					// Increase the packet counter.
					this.broadcastedPacketCounter += 1;

					// Log that we have received a packet.
					debug.network(`Received broadcasted packet #${this.broadcastedPacketCounter} with topic ${topic}.`);
					debug.object(content);

					try
					{
						// Parse the received packet.
						const parsedContent = await OracleData.unpackOracleMessage(hexToBin(`${topic}${content}`));

						// Send the packet topic and content to the callback function.
						await callback(topic.toString(), parsedContent);
					}
					catch(error)
					{
						// Log that we failed to process this packet.
						debug.warning(`Failed to process broadcasted packet #${this.broadcastedPacketCounter} with topic ${topic}.`);
					}
				}

				// Write a notification that the connection was terminated.
				debug.network('Socket used in receivePackets() was closed.');
			}
			catch(error)
			{
				// Do nothing and continue to receive packets.
				debug.warning('Failed to continue processing data in receivePackets()');
			}

			// If we should reconnect..
			if(this.shouldReconnectWithBroadcast[socketUrl])
			{
				// Log that reconnection will be attempted soon.
				debug.network(`Attempting to restore connection with ${address}:${port} in order to listen to broadcasts in 250ms.`);

				// Wait 250ms to prevent excessively rapid reconnection.
				await new Promise((resolve) => setTimeout(resolve, 250));
			}
		}
	}

	/**
	 * Creates a network connection and registers a callback to handle broadcasted messages.
	 *
	 * @param callback  {function}   a function that will handle incoming packets, taking topic and content as function parameters.
	 * @param address   {string}     a hostname or IP address which the broadcast socket will bind to.
	 * @param port      {number}     a port number which the broadcast socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async listenToBroadcasts(callback: IncomingPacketCallback, address: string, port: number = OracleProtocol.PORT_BROADCAST): Promise<void>
	{
		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		// Initialize broadcastSubscriber list, if necessary.
		if(!this.broadcastSubscriber)
		{
			this.broadcastSubscriber = {};
		}

		// Verify that we are not already listening for message from this oracle.
		if(this.broadcastSubscriber[socketUrl])
		{
			throw(new Error('Cannot register for broadcast messages from the same oracle multiple times.'));
		}

		// Start listening to broadcasted messages.
		this.receiveBroadcastPackets(callback, address, port);
	}

	/**
	 * Shuts down the network connection listening to broadcasted packages for a given address and port.
	 *
	 * @param address   {string}     a hostname or IP address which the broadcast socket will bind to.
	 * @param port      {number}     a port number which the broadcast socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async stopListeningToBroadcasts(address: string, port: number = OracleProtocol.PORT_BROADCAST): Promise<void>
	{
		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		// Log a warning if there is no broadcast connection to stop listening to.
		if(!this.shouldReconnectWithBroadcast[socketUrl])
		{
			debug.warning(`Cannot disable listening for broadcasted messages on ${socketUrl} when not active.`);

			return;
		}

		// Log that we will no longer listen to relayed messages.
		debug.action(`Shutting down connection listening to broadcasted messages from ${socketUrl}.`);

		// Set the reconnection flag to false to prevent automatic reconnections.
		this.shouldReconnectWithBroadcast[socketUrl] = false;

		// Close the socket.
		this.broadcastSubscriber[socketUrl].close();

		// Remove the reference to the broadcast socket.
		delete this.broadcastSubscriber[socketUrl];
	}

	/**
	 * Creates and binds a broadcast socket to the network parameters.
	 *
	 * @param address   {string}   a hostname or IP address which the broadcast socket will bind to.
	 * @param port      {number}   a port number which the broadcast socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async setupBroadcasting(address: string = OracleProtocol.ADDRESS_ANY, port: number = OracleProtocol.PORT_BROADCAST): Promise<void>
	{
		// Verify that we have not already enabled broadcasting.
		if(this.broadcastPublisher)
		{
			throw(new Error('Cannot setup broadcasting when already active.'));
		}

		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		try
		{
			// Create a publisher socket.
			this.broadcastPublisher = new Publisher();

			// Bind to the address and port configured.
			await this.broadcastPublisher.bind(socketUrl);

			// Log that we are now setup to broadcast messages.
			debug.action(`Prepared channel to broadcast messages on ${address}:${port}.`);
		}
		catch(error: any)
		{
			// Throw an error message explaining that we failed to set up the broadcast socket.
			throw(new Error(`Failed to create broadcast socket on ${socketUrl}. (${error.toString()})`));
		}
	}

	/**
	 * Tears down broadcasting support and release related resources.
	 *
	 * @param address   {string}   a hostname or IP address which the broadcast socket is bound to.
	 * @param port      {number}   a port number which the broadcast socket is bound to.
	 *
	 * @returns {Promise<void>}
	 */
	static async shutdownBroadcasting(address: string = OracleProtocol.ADDRESS_ANY, port: number = OracleProtocol.PORT_BROADCAST): Promise<void>
	{
		// Verify that we have have enabled broadcasting.
		if(!this.broadcastPublisher)
		{
			debug.warning('Ignoring request to shut down inactive broadcasting as broadcasting is not active.');

			return;
		}

		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		try
		{
			// Close the broadcast published socket.
			this.broadcastPublisher.close();

			// Delete the reference to the socket.
			delete this.broadcastPublisher;

			// Log that we are now setup to broadcast messages.
			debug.action(`Shut down channel to broadcast messages for ${address}:${port}.`);
		}
		catch(error: any)
		{
			// Throw an error message explaining that we failed to take down the broadcast socket.
			throw(new Error(`Failed to shut down broadcast socket for ${socketUrl}. (${error.toString()})`));
		}
	}

	/**
	 * Distributes a message with a given topic to connected peers.
	 *
	 * @param topic     {string}   topic for the message.
	 * @param content   {string}   content of the message.
	 *
	 * @return  {Promise<void>}
	 */
	static async broadcast(topic: string, content: string): Promise<void>
	{
		// Verify that we have initialize a broadcast connection.
		if(!this.broadcastPublisher)
		{
			throw(new Error('Cannot broadcast before initializing a broadcast connection.'));
		}

		// Get a mutex to prevent concurrent attempts to send to the socket.
		const unlockSend = await sendLock.acquire();

		try
		{
			// Log that we're broadcasting a message.
			debug.network(`Broadcasting a '${topic}' message.`);
			debug.object(content);

			// Send the broadcast message to the socket.
			return await this.broadcastPublisher.send([ topic, content ]);
		}
		catch(error: any)
		{
			// Re-throw error with additional context.
			throw(new Error(`Failed to broadcast the '${topic}' message: ${error}`));
		}
		finally
		{
			// Always release the lock
			unlockSend();
		}
	}

	/**
	 * Distributes a price message to our connected peers.
	 *
	 * @param message     {Uint8Array}   price message to distribute, as a Uint8Array.
	 * @param signature   {Uint8Array}   message signature to verify, as a Uint8Array.
	 * @param publicKey   {Uint8Array}   public key for the signature, as a Uint8Array.
	 *
	 * @return {Promise<void>}
	 */
	static async broadcastPriceMessage(message: Uint8Array, signature: Uint8Array, publicKey: Uint8Array): Promise<void>
	{
		// create the topic and content to broadcast.
		const broadcastTopic = binToHex(await OracleData.createPacketTopic());
		const broadcastContent = binToHex(await OracleData.createPacketContent(message, publicKey, signature));

		// Return the broadcast promise.
		return this.broadcast(broadcastTopic, broadcastContent);
	}

	/**
	 * Creates and binds a relay socket to the network parameters.
	 *
	 * @param address   {string}   a hostname or IP address which the relay socket will bind to.
	 * @param port      {number}   a port number which the relay socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async setupRelay(address: string, port: number = OracleProtocol.PORT_RELAY): Promise<void>
	{
		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		// Initialize relayPublishers list, if necessary.
		if(!this.relayPublishers)
		{
			this.relayPublishers = {};
		}

		// Verify that we are not already relaying messages to this oracle.
		if(this.relayPublishers[socketUrl])
		{
			throw(new Error('Cannot setup to relay messages to the same oracle multiple times.'));
		}

		try
		{
			// Create a publisher socket.
			this.relayPublishers[socketUrl] = new Publisher();

			// Bind to the address and port configured.
			await this.relayPublishers[socketUrl].connect(socketUrl);

			// Log that we are now setup to relay messages.
			debug.action(`Prepared channel to relay messages to ${address}:${port}.`);
		}
		catch(error: any)
		{
			// Throw an error message explaining that we failed to set up the broadcast socket.
			throw(new Error(`Failed to create relay socket on ${socketUrl}. (${error.toString()})`));
		}
	}

	/**
	 * Tears down support for relaying messages to peers.
	 *
	 * @param address   {string}   a hostname or IP address which the relay socket is bound to.
	 * @param port      {number}   a port number which the relay socket is bound to.
	 *
	 * @returns {Promise<void>}
	 */
	static async shutdownRelay(address: string, port: number = OracleProtocol.PORT_RELAY): Promise<void>
	{
		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		// Initialize relayPublishers list, if necessary.
		if(!this.relayPublishers)
		{
			this.relayPublishers = {};
		}

		// Verify that we are actually relaying messages to this oracle.
		if(!this.relayPublishers[socketUrl])
		{
			throw(new Error('Cannot shut down relay of messages for an oracle we are not relaying to.'));
		}

		try
		{
			// Shut down the network connection.
			this.relayPublishers[socketUrl].close();

			// Delete the reference to the socket.
			delete this.relayPublishers[socketUrl];

			// Log that we are now setup to relay messages.
			debug.action(`Shut down channel to relay messages to ${address}:${port}.`);
		}
		catch(error: any)
		{
			// Throw an error message explaining that we failed to set up the broadcast socket.
			throw(new Error(`Failed to shut down relay socket on ${socketUrl}. (${error.toString()})`));
		}
	}

	/**
	 * Creates a network connection and registers a callback to handle relayed messages.
	 *
	 * @param callback  {function}   a function that will handle incoming packets, taking topic and content as function parameters.
	 * @param address   {string}     a hostname or IP address which the relay socket will bind to.
	 * @param port      {number}     a port number which the relay socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async listenToRelays(callback: IncomingPacketCallback, address: string = OracleProtocol.ADDRESS_LOCAL, port: number = OracleProtocol.PORT_RELAY): Promise<void>
	{
		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		// Throw an error if we have already set up listening to relays.
		if(this.relaySubscriber)
		{
			throw(new Error('Cannot enable listening for relay messages when already active.'));
		}

		// Initialize a packet counter.
		this.relayedPacketCounter = 0;

		// Initialize a boolean controlling when to stop listening to relays.
		this.shouldReconnectWithRelays = true;

		// Continuously reconnect and listen to relay until toggled off.
		while(this.shouldReconnectWithRelays)
		{
			// Create a subscriber socket.
			this.relaySubscriber = new Subscriber();

			// Start accepting connections from peers who want their messages relayed.
			await this.relaySubscriber.bind(socketUrl);

			// Subscribe to all messages.
			await this.relaySubscriber.subscribe();

			// Log that we are now listening for relayed messages.
			debug.action(`Listening for relayed messages on ${address}:${port}.`);

			// Wrap in try-catch to ensure that errors does not interrupt service.
			try
			{
				// Iterate over incoming packets until the connection is broken.
				for await (const [ topic, content ] of this.relaySubscriber)
				{
					// Increase the packet counter.
					this.relayedPacketCounter += 1;

					// Log that we have received a packet.
					debug.network(`Received relayed packet #${this.relayedPacketCounter} with topic ${topic}.`);
					debug.object(content);

					try
					{
						// Parse the request.
						const parsedContent = await OracleData.unpackOracleMessage(hexToBin(`${topic}${content}`));

						// Send the packet topic and content to the callback function.
						await callback(topic.toString(), parsedContent);
					}
					catch(error)
					{
						// Log that we failed to process this packet.
						debug.warning(`Failed to process relayed packet #${this.relayedPacketCounter} with topic ${topic}.`);
					}
				}

				// Write a notification that the connection was terminated.
				debug.network('Socket used in listenToRelays() was closed.');
			}
			catch(error)
			{
				// Do nothing and continue to receive packets.
				debug.warning('Failed to continue processing data in listenToRelays()');
			}

			// If we should reconnect..
			if(this.shouldReconnectWithRelays)
			{
				// Log that reconnection will be attempted soon.
				debug.network('Attempting to restore connection in order to listen to relays in 250ms.');

				// Wait 250ms to prevent excessively rapid reconnection.
				await new Promise((resolve) => setTimeout(resolve, 250));
			}
		}
	}

	/**
	 * Shuts down the network connection listening to relayed packages.
	 *
	 * @returns {Promise<void>}
	 */
	static async stopListeningToRelays(): Promise<void>
	{
		// Log a warning if there is no relay connection to stop listening to.
		if(!this.relaySubscriber || !this.shouldReconnectWithRelays)
		{
			debug.warning('Ignoring request to stop listening to relays as relay listener is not active.');

			return;
		}

		// Log that we will no longer listen to relayed messages.
		debug.action('Shutting down connection listening to relayed messages.');

		// Set the reconnection flag to false to prevent automatic reconnections.
		this.shouldReconnectWithRelays = false;

		// Close the socket.
		this.relaySubscriber.close();

		// Delete the reference to the relay socket.
		delete this.relaySubscriber;
	}

	/**
	 * Relays a message with a given topic to connected relay peers.
	 *
	 * @param topic     {string}   topic for the message.
	 * @param content   {string}   content of the message, as a triple of message/publicKey/signature.
	 *
	 * @throws {Error} If any of the relay messages fail to send.
	 *
	 * @return {Promise<Array<void>>} A Promise.all() containing each relay send result.
	 */
	static async relay(topic: string, content: string): Promise<Array<void>>
	{
		// Verify that we have initialize a broadcast connection.
		if(!this.relayPublishers)
		{
			throw(new Error('Cannot broadcast before initializing a broadcast connection.'));
		}

		// Log that we're broadcasting a message.
		debug.network(`Relaying a '${topic}' message.`);
		debug.object(content);

		// Create a list to track relay promises.
		const relayPromises = [];

		// For each connected relay peer..
		for(const relayPublisher in this.relayPublishers)
		{
			// .. relay the message.
			relayPromises.push(this.relayPublishers[relayPublisher].send([ topic, content ]));
		}

		// Return the relay promises.
		return Promise.all(relayPromises);
	}

	/**
	 * Relays a price message to our connected relay peers.
	 *
	 * @param message     {Uint8Array}   price message to relay, as a Uint8Array.
	 * @param signature   {Uint8Array}   message signature to verify, as a Uint8Array.
	 * @param publicKey   {Uint8Array}   public key for the signature, as a Uint8Array.
	 *
	 * @throws {Error} If any of the price relay messages fail to send.
	 *
	 * @return {Promise<Array<void>>} A Promise.all() containing each relay send result.
	 */
	static async relayPriceMessage(message: Uint8Array, signature: Uint8Array, publicKey: Uint8Array): Promise<Array<void>>
	{
		// create the topic and content to relay.
		const relayTopic = binToHex(await OracleData.createPacketTopic());
		const relayContent = binToHex(await OracleData.createPacketContent(message, publicKey, signature));

		// Return the broadcast promise.
		return this.relay(relayTopic, relayContent);
	}

	/**
	 * Creates and initializes an incoming request socket.
	 *
	 * @param callback  {function}   a function that will handle incoming requests, taking content as function parameter.
	 * @param address   {string}     a hostname or IP address which the request socket will bind to.
	 * @param port      {number}     a port number which the request socket will bind to.
	 *
	 * @returns {Promise<void>}
	 */
	static async listenToRequests(callback: IncomingRequestCallback, address: string = OracleProtocol.ADDRESS_ANY, port: number = OracleProtocol.PORT_REQUEST): Promise<void>
	{
		// Throw an error if we have already set up listening to requests.
		if(this.replySocket)
		{
			throw(new Error('Cannot enable listening for request messages when reply socket is already active.'));
		}

		// Craft a socket URL from the parameters provided.
		const socketUrl = this.getSocketUrl(address, port);

		// Initialize a packet counter.
		this.requestPacketCounter = 0;

		// Initialize a boolean controlling when to stop listening for requests.
		this.shouldReconnectWithRequests = true;

		// Continuously reconnect and listen to requests until toggled off.
		while(this.shouldReconnectWithRequests)
		{
			// Create a reply socket.
			this.replySocket = new Reply();

			// Start accepting connections from peers who want request data.
			await this.replySocket.bind(socketUrl);

			// Log that we are now setup to get requests.
			debug.action(`Listening for requests on ${address}:${port}.`);

			// Wrap in try-catch to ensure that errors does not interrupt service.
			try
			{
				// Iterate over incoming requests until the connection is broken.
				for await (const [ content ] of this.replySocket)
				{
					// Increase the packet counter.
					this.requestPacketCounter += 1;

					// Log that we have received a packet.
					debug.network(`Received request packet #${this.requestPacketCounter}.`);
					debug.object(content);

					try
					{
						// Parse the request.
						const parsedContent = JSON.parse(content.toString(), reviver);

						// Send the packet topic and content to the callback function.
						await callback(parsedContent);
					}
					catch(error)
					{
						// Log that we failed to process this packet.
						debug.warning(`Failed to process request packet #${this.requestPacketCounter}.`);
					}
				}

				// Write a notification that the connection was terminated.
				debug.network('Socket used in listenToRequests() was closed.');
			}
			catch(error)
			{
				// Do nothing and continue to receive packets.
				debug.warning('Failed to continue processing data in listenToRequests()');
			}

			// If we should reconnect..
			if(this.shouldReconnectWithRequests)
			{
				// Log that reconnection will be attempted soon.
				debug.network('Attempting to restore connection in order to listen to requests in 250ms.');

				// Wait 250ms to prevent excessively rapid reconnection.
				await new Promise((resolve) => setTimeout(resolve, 250));
			}
		}
	}

	/**
	 * Shuts down the network connection listening to request packages.
	 *
	 * @returns {Promise<void>}
	 */
	static async stopListeningToRequests(): Promise<void>
	{
		// Log a warning if there is no request connection to stop listening to.
		if(!this.replySocket || !this.shouldReconnectWithRequests)
		{
			debug.warning('Ignoring request to stop listening for requests as we are not listening for requests.');

			return;
		}

		// Log that we will no longer listen to request messages.
		debug.action('Shutting down connection listening to request messages.');

		// Set the reconnection flag to false to prevent automatic reconnections.
		this.shouldReconnectWithRequests = false;

		// Close the socket.
		this.replySocket.close();

		// Delete the reference to the request socket.
		delete this.replySocket;
	}

	/**
	 * Sends a request for data to an oracle peer.
	 *
	 * @param content        {any}      the request content, as an entity that can be JSON stringified.
	 * @param address        {string}   a hostname or IP address which to send the request to.
	 * @param port           {number}   a port number which the request will be sent on.
	 * @param sendTimeout    {number}   number of milliseconds to wait when trying to send before considering the request failed.
	 * @param receiveTimeout {number}   number of milliseconds to wait for a response before considering the request failed.
	 *
	 * NOTE: Timeout settings are not updated on subsequent requests, even if changed, as we re-use existing connections to keep down latency.
	 *
	 * @returns {Promise<any>} the response of the request.
	 */
	static async request(content: any, address: string, port: number = OracleProtocol.PORT_REQUEST, sendTimeout = OracleProtocol.DEFAULT_SEND_TIMEOUT, receiveTimeout = OracleProtocol.DEFAULT_RECEIVE_TIMEOUT): Promise<any>
	{
		// Get a mutex to prevent concurrent attempts to create or send to the socket.
		const unlockRequest = await requestLock.acquire();

		try
		{
			// Craft a socket URL from the parameters provided.
			const socketUrl = this.getSocketUrl(address, port);

			// Initialize request socket list, if necessary.
			if(!this.requestSockets)
			{
				this.requestSockets = {};
			}

			// Initialize request socket, if necessary.
			if(!this.requestSockets[socketUrl])
			{
				// Create the socket.
				this.requestSockets[socketUrl] = new Request({ sendTimeout, receiveTimeout });

				// Connect with the peer.
				await this.requestSockets[socketUrl].connect(socketUrl);
			}

			// Log that we are sending a reply.
			debug.network(`Sending request to ${address}:${port}.`);

			// Send the request.
			// NOTE: Rejections/failures here are unhandled so they propagate upstream.
			//       This can reject/fail when we cannot schedule outgoing messages:
			//       - device is offline
			//       - device is out of memory.
			//       - send timed out according to request setting.
			// NOTE: It will not reject/fail if outgoing message queue is full, instead it retry indefinitely.
			await this.requestSockets[socketUrl].send(JSON.stringify(content, replacer));

			// Declare a result variable to keep it correctly scoped.
			let result;

			try
			{
				// Wait for the response..
				// NOTE: This can reject/fail when we cannot receive incoming messages:
				//       - device is offline
				//       - device is out of memory
				//       - incoming message queue is full
				//       - receive timed out according to request setting.
				[ result ] = await this.requestSockets[socketUrl].receive();
			}
			catch(error)
			{
				// Extract the socket reference so that failures when closing the socket won't leave it in cache.
				// NOTE: the local copy will be deleted when we go out of scope, and actual socket will be garbage collected.
				const localCopyOfSocket = this.requestSockets[socketUrl];
				delete this.requestSockets[socketUrl];

				// Teardown the connection
				localCopyOfSocket.close();

				// Define an error message.
				const errorMessage = `Failed to get a response for request sent to ${address}:${port}: ${error}`;

				// Log and then throw an error and let upstream handle the situation.
				debug.warning(errorMessage);
				throw(new Error(errorMessage));
			}

			try
			{
				// Parse the response.
				const parsedResponse = JSON.parse(result.toString(), reviver);

				// Return the parsed response.
				return parsedResponse;
			}
			catch(error)
			{
				// Log that we failed to process the response.
				debug.warning(`Failed to process response for request sent to ${address}:${port}.`);

				// Return undefined to indicate error.
				return undefined;
			}
		}
		catch(error)
		{
			// Re-throw the error with additional context.
			throw(new Error(`Failed to send request to ${address}:${port}: ${error}`));
		}
		finally
		{
			// Remove the request mutex lock.
			unlockRequest();
		}
	}

	/**
	 * Replies to the current incoming request
	 *
	 * @param data   {any}   the data that we should send in the reply.
	 *
	 * @returns {Promise<void>}
	 */
	static async reply(data: any): Promise<void>
	{
		// Verify than reply socket is active
		if(!this.replySocket)
		{
			throw(new Error('Cannot send a reply as reply socket is not active'));
		}

		// Verify that we can send a reply, and if not..
		if(!this.replySocket.writable)
		{
			// Throw an error as this breaks the assumption that we have gotten a request to reply to.
			throw(new Error('!! Throwing error since it is not possible to reply when there is no active request to reply to.'));
		}

		// Log that we are sending a reply.
		debug.network(`Sending reply to request #${this.requestPacketCounter}.`);
		debug.object(data);

		// Send the data in as a JSON encoded string.
		this.replySocket.send(JSON.stringify(data, replacer));
	}
}
