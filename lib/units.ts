/**
 * Utility function to create a string from a number, rounded to the given number of decimal places.
 *
 * @function
 *
 * @param numberToFormat {number} The number to round.
 * @param decimalPlaces  {number} The number of decimal places to show.
 *
 * @returns {string} A string of the number rounded to the given number of decimals.
 */
const formatNumberToDigits = function(numberToFormat: number, decimalPlaces: number = 0): string
{
	// Set the options of the Number Format object.
	const options =
	{
		style: 'decimal',
		minimumFractionDigits: decimalPlaces,
		maximumFractionDigits: decimalPlaces,
		useGrouping: false,
		numberingSystem: 'latn',
	};

	// Create an instance of number format using above options.
	// NOTE: We force the locale to en-GB so that the number is formatted in a javascript-friendly format with a decimal, not a comma.
	const numberFormat = new Intl.NumberFormat('en-GB', options);

	// Format the number to a string according to specification.
	const formattedAmount = numberFormat.format(numberToFormat);

	return formattedAmount;
};

/**
 * Bitcoin Cash Units Primitive.
 *
 * Support class used to validate and convert between satoshis and BCH.
 */
export class Satoshis
{
	// All interpretations of this class are based on the satoshis value.
	public readonly satoshis: bigint;

	// ...
	public static PER_BCH = 100_000_000;

	//
	// Constructor and various ways to instantiate from related values
	//

	/**
	 * Construct a new instance of Satoshi Units.
	 *
	 * @param amountInSatoshis   {number} Amount of Bitcoin Cash, in satoshis.
	 * @throws {Error} if either input does not validate.
	 */
	constructor(amountInSatoshis: bigint | number)
	{
		// If a number is provided...
		if(typeof amountInSatoshis === 'number')
		{
			// Validate the provided value.
			Satoshis.validateAmountInSatoshis(amountInSatoshis);
		}

		// Cast the amount to a bigint and set our satoshi primitive.
		// NOTE: BigInt constructor will throw when given a non-integer value.
		this.satoshis = BigInt(amountInSatoshis);
	}

	/**
	 * Instantiates from Satoshi Amount.
	 *
	 * @param amountInSats {bigint|number} Amount of satoshis.
	 *
	 * @returns {Satoshis} New instance of Satoshi Units.
	 */
	static fromSats(amountInSats: bigint | number): Satoshis
	{
		// Create the instance
		const satoshiUnits = new Satoshis(amountInSats);

		return satoshiUnits;
	}

	/**
	 * Instantiates from a BCH amount.
	 *
	 * @param amountInBch   {number} Amount of Bitcoin Cash, in BCH.
	 *
	 * @returns {Satoshis} New instance of Satoshi Units.
	 */
	static fromBch(amountInBch: number): Satoshis
	{
		// Validate the common unit value. Others are validated by OracleUnit instantiation.
		Satoshis.validateAmountInBch(amountInBch);

		// Get the satoshi amount
		const amountInSatoshis = Math.round(amountInBch * Satoshis.PER_BCH);

		// Create the instance
		const satoshiUnits = new Satoshis(amountInSatoshis);

		return satoshiUnits;
	}

	//
	// Computed interpretations of the underlying value
	//

	/**
	 * Return the amount in satoshis (as a bigint).
	 *
	 * @returns {bigint} The amount in satoshis.
	 */
	toSats(): bigint
	{
		// Return the base unit amount
		return this.satoshis;
	}

	/**
	 * Return the amount in satoshis.
	 *
	 * @deprecated It is recommended to use toSats() instead.
	 *
	 * @returns {number} The amount in satoshis.
	 */
	toSatoshis(): number
	{
		// Return the base unit amount
		return Number(this.satoshis);
	}

	/**
	 * Return the amount in BCH.
	 *
	 * @returns {number} The amount in BCH.
	 */
	toBch(): number
	{
		// Calculate the amount in BCH.
		const amountInBch = Number(this.satoshis) / Satoshis.PER_BCH;

		// round it to at most 8 digit string and convert back to a number
		const formattedAmountInBch = Number(formatNumberToDigits(amountInBch, 8));

		return formattedAmountInBch;
	}

	//
	// Internal validators
	//

	/**
	 * Validate an amount of satoshis.
	 *
	 * @param amountInSatoshis {number} Amount of satoshis.
	 * @throws {Error} if the input does not validate.
	 */
	static validateAmountInSatoshis(amountInSatoshis: number): void
	{
		// Validate as safe javascript integer
		if(!Number.isSafeInteger(amountInSatoshis))
		{
			throw(new Error(`Expected satoshis to be a safe javascript integer but got ${amountInSatoshis}.`));
		}
	}

	/**
	 * Validate an amount of BCH.
	 *
	 * @param amountInBch {number} Amount of BCH.
	 * @throws {Error} if the input does not validate.
	 */
	static validateAmountInBch(amountInBch: number): void
	{
		// Validate as finite javascript number
		if(!Number.isFinite(amountInBch))
		{
			throw(new Error(`Expected amountInBch to be a finite javascript number but got ${amountInBch}.`));
		}

		// Convert to an 8-digit amount for a round-trip test.
		const BchWith8Digits = Number(formatNumberToDigits(amountInBch, 8));

		// Check if the 8-digit amount is equal to the original amount
		if(BchWith8Digits !== amountInBch)
		{
			throw(new Error(`Expected amountInBch amount to be a number with at most 8 decimal digits but got ${amountInBch}`));
		}
	}
}

/**
 * Oracle Units Primitive.
 *
 * When working with a price oracle, the units are not always intuitive, and it is easy to make mistakes in
 * rounding, integer requirements, and other subtle details. This class increases both convenience
 * and safety when working with values and prices related to a price oracle.
 */
export class OracleUnits
{
	// All interpretations of this class are based on these two immutable values:
	public readonly oracleUnits: number;
	public readonly oracleUnitsPerCommonUnit: number;

	//
	// Constructor and various ways to instantiate from related values
	//

	/**
	 * Construct a new instance of Oracle Units.
	 *
	 * @param amountInOracleUnits      {number} Amount of oracle units.
	 * @param oracleUnitsPerCommonUnit {number} Scaling between oracle and common units. Equivalent to "attestation scaling" in the oracle specification.
	 * @throws {Error} if either input does not validate.
	 */
	constructor(amountInOracleUnits: number, oracleUnitsPerCommonUnit: number)
	{
		// Validate that the provided values are both strictly in oracle specification.
		// Note that the inconvenient strict requirement is important to avoid hiding both major and minor bugs.
		// For example, accepting non-integers and silently rounding them will create a disconnect between user
		// expectations and reality.
		// Alternatively, accepting and storing non-integers leads to undefined behavior from the perspective
		// of the price oracle specification.
		OracleUnits.validateAmountInOracleUnits(amountInOracleUnits);
		OracleUnits.validateOracleUnitsPerCommonUnit(oracleUnitsPerCommonUnit);

		// Assign the validated inputs to the instance
		this.oracleUnits = amountInOracleUnits;
		this.oracleUnitsPerCommonUnit = oracleUnitsPerCommonUnit;
	}

	/**
	 * Instantiates from a Common Unit Amount.
	 *
	 * @param commonUnits              {number} The common units amount.
	 * @param oracleUnitsPerCommonUnit {number} Scaling between oracle and common units. Equivalent to "attestation scaling" in the oracle specification.
	 *
	 * @returns {OracleUnits} New instance of Oracle Units.
	 */
	static fromCommonUnits(commonUnits: number, oracleUnitsPerCommonUnit: number): OracleUnits
	{
		// Validate the common unit value. Others are validated by OracleUnit instantiation.
		OracleUnits.validateCommonUnitsAmount(commonUnits);

		// Get the (rounded) integer oracle units
		const oracleUnitAmount = Math.round(commonUnits * oracleUnitsPerCommonUnit);

		// Create the instance
		const oracleUnits = new OracleUnits(oracleUnitAmount, oracleUnitsPerCommonUnit);

		return oracleUnits;
	}

	/**
	 * Instantiates from a whole (integer) satoshi amount.
	 *
	 * @param satoshis                 {number} The whole (integer) satoshi amount.
	 * @param oracleUnitsPerCommonUnit {number} Scaling between oracle and common units. Equivalent to "attestation scaling" in the oracle specification.
	 * @param priceInOracleUnitsPerBch {number} The Price (in Oracle Units per BCH) to use to convert from satoshis
	 *
	 * @returns {OracleUnits} New instance of Oracle Units.
	 */
	static fromSatoshis(satoshis: number, oracleUnitsPerCommonUnit: number, priceInOracleUnitsPerBch: number): OracleUnits
	{
		// Validate the satoshi value.
		Satoshis.validateAmountInSatoshis(satoshis);

		// Validate the price. Others are validated by OracleUnit instantiation.
		OracleUnits.validatePriceInOracleUnitsPerBch(priceInOracleUnitsPerBch);

		// Convert the satoshis to BCH.
		const amountInSatoshis = new Satoshis(satoshis);
		const amountInBch = amountInSatoshis.toBch();

		// Calculate the unit value
		const amountInOracleUnits = Math.round(amountInBch * priceInOracleUnitsPerBch);

		// Create the instance
		const oracleUnits = new OracleUnits(amountInOracleUnits, oracleUnitsPerCommonUnit);

		return oracleUnits;
	}

	/**
	 * Instantiates from a BCH Amount.
	 *
	 * @param amountInBch                      {number} The BCH amount.
	 * @param oracleUnitsPerCommonUnit {number} Scaling between oracle and common units. Equivalent to "attestation scaling" in the oracle specification.
	 * @param priceInOracleUnitsPerBch {number} The Price (in Oracle Units per BCH) to use to convert from Satoshis
	 *
	 * @returns {OracleUnits} New instance of Oracle Units.
	 */
	static fromBch(amountInBch: number, oracleUnitsPerCommonUnit: number, priceInOracleUnitsPerBch: number): OracleUnits
	{
		// Validate the BCH amount. Others are validated by OracleUnit instantiation.
		Satoshis.validateAmountInBch(amountInBch);

		// Validate the price. Others are validated by OracleUnit instantiation.
		OracleUnits.validatePriceInOracleUnitsPerBch(priceInOracleUnitsPerBch);

		// Calculate the unit amount
		const oracleUnitAmount = Math.round(amountInBch * priceInOracleUnitsPerBch);

		// Create the instance
		const oracleUnits = new OracleUnits(oracleUnitAmount, oracleUnitsPerCommonUnit);

		return oracleUnits;
	}

	//
	// Computed interpretations of the underlying value
	//

	/**
	 * Return the amount in Oracle Units.
	 *
	 * @returns {number} The amount in Oracle Units.
	 */
	toOracleUnits(): number
	{
		// Return the base unit amount
		return this.oracleUnits;
	}

	/**
	 * Return the amount in Common Units.
	 *
	 * @returns {number} The amount in Common Units.
	 */
	toCommonUnits(): number
	{
		// Get the full precision value
		const fullPrecisionCommonUnits = this.oracleUnits / this.oracleUnitsPerCommonUnit;

		return fullPrecisionCommonUnits;
	}

	/**
	 * Return the amount in common units, formatted as a string with configurable decimal points.
	 *
	 * @param roundToDigits {number | 'round_to_display'} Directive to format with general-rounding or display-rounding.
	 *
	 * @returns {string} The common units value as a formatted string.
	 */
	toCommonUnitsFormatted(roundToDigits: number | 'round_to_display' = 'round_to_display'): string
	{
		// Get the full precision value
		const fullPrecisionCommonUnits = this.oracleUnits / this.oracleUnitsPerCommonUnit;

		// Figure out how many digits to round
		let finalRoundingDigits: number;

		// Interpret the overloaded argument to determine how many digits to round
		if(roundToDigits === 'round_to_display')
		{
			// Use the internal heuristic for number of digits to preserve.
			// Note that if you follow the math on this formatting, it *seems* to do nothing, but it
			// overcomes the cases where a calculation in floating point sometimes results in stray very small amounts.
			finalRoundingDigits = this.getDecimalPlacesForCommonUnitDisplay();
		}
		else
		{
			// By default, use the input as just a number.
			finalRoundingDigits = roundToDigits;
		}

		// Format the number to the specified digits
		const commonUnitsFormatted = formatNumberToDigits(fullPrecisionCommonUnits, finalRoundingDigits);

		return commonUnitsFormatted;
	}

	/**
	 * Convert the amount to whole Satoshis, calculated using a given price in Oracle Units.
	 *
	 * @param priceInOracleUnitsPerBch {number} The Price (in Oracle Units per BCH) to use to convert from Satoshis
	 *
	 * @returns {number} The amount in Satoshis.
	 */
	toSatoshis(priceInOracleUnitsPerBch: number): number
	{
		// Validate the price.
		OracleUnits.validatePriceInOracleUnitsPerBch(priceInOracleUnitsPerBch);

		// Calculate the oracle price in common units (BCH).
		const amountInBch = (this.oracleUnits / priceInOracleUnitsPerBch);

		// Convert the amount in BCH to a satoshi value.
		// NOTE: We round to a fixed number of decimals to ensure a valid BCH value.
		const satoshis = Satoshis.fromBch(Number(formatNumberToDigits(amountInBch, 8)));

		return satoshis.toSatoshis();
	}

	/**
	 * Convert the amount to BCH, calculated using a given price in Oracle Units.
	 *
	 * @param priceInOracleUnitsPerBch {number} The Price (in Oracle Units per Bch) to use to convert from Satoshis
	 *
	 * @returns {number} The amount in BCH.
	 */
	toBch(priceInOracleUnitsPerBch: number): number
	{
		// Validate the price.
		OracleUnits.validatePriceInOracleUnitsPerBch(priceInOracleUnitsPerBch);

		// Calculate the oracle price in common units (BCH).
		const amountInBch = (this.oracleUnits / priceInOracleUnitsPerBch);

		// NOTE: We round to a fixed number of decimals to ensure a valid BCH value.
		const satoshis = Satoshis.fromBch(Number(formatNumberToDigits(amountInBch, 8)));

		return satoshis.toBch();
	}

	//
	// Internal utilities
	//

	/**
	 * Get the conventional number of decimal places that should be used when displaying the amount in Common Units.
	 *
	 * @returns {number} Number of decimal places that should be used when displaying in Common Units.
	 */
	getDecimalPlacesForCommonUnitDisplay(): number
	{
		// Determine the number of decimal places by checking the length of the unit conversion (as a string) and subtracting one from it.
		// Note that this is a practical heuristic that works well enough by experimentation.
		const decimalPlaces = (this.oracleUnitsPerCommonUnit.toString().length - 1);

		return decimalPlaces;
	}

	//
	// Internal validators
	//

	/**
	 * Validate an amount of oracle units.
	 *
	 * @param oracleUnitsAmount {number} Amount of oracle units.
	 * @throws {Error} if the input does not match price oracle specification.
	 */
	static validateAmountInOracleUnits(oracleUnitsAmount: number): void
	{
		// Validate as safe javascript integer
		if(!Number.isSafeInteger(oracleUnitsAmount))
		{
			throw(new Error(`Expected oracleUnitsAmount to be a safe javascript integer but got ${oracleUnitsAmount}.`));
		}
	}

	/**
	 * Validate oracleUnitsPerCommonUnit, also known as attestation scaling according to price oracle specification.
	 *
	 * @param oracleUnitsPerCommonUnit {number} Scaling between oracle and common units. Equivalent to "attestation scaling" in the oracle specification.
	 * @throws {Error} if the input does not match price oracle specification.
	 */
	static validateOracleUnitsPerCommonUnit(oracleUnitsPerCommonUnit: number): void
	{
		// Validate as safe javascript integer
		if(!Number.isSafeInteger(oracleUnitsPerCommonUnit))
		{
			throw(new Error(`Expected oracleUnitsPerCommonUnit to be a safe javascript integer but got ${oracleUnitsPerCommonUnit}.`));
		}

		// Validate as positive
		if(!(oracleUnitsPerCommonUnit > 0))
		{
			throw(new Error(`Expected oracleUnitsPerCommonUnit to be positive but got ${oracleUnitsPerCommonUnit}.`));
		}
	}

	/**
	 * Validate an amount of common units.
	 *
	 * @param commonUnitsAmount {number} Amount of common units.
	 * @throws {Error} if the input does not validate.
	 */
	static validateCommonUnitsAmount(commonUnitsAmount: number): void
	{
		// Validate as a finite javascript number
		if(!Number.isFinite(commonUnitsAmount))
		{
			throw(new Error(`Expected commonUnitsAmount to be a finite javascript Number but got ${commonUnitsAmount}.`));
		}
	}

	/**
	 * Validate an oracle price.
	 *
	 * @param oracleUnitsPerBch {number} Price as oracle units per BCH.
	 * @throws {Error} if the input does not match price oracle specification.
	 */
	static validatePriceInOracleUnitsPerBch(oracleUnitsPerBch: number): void
	{
		// Validate as safe javascript integer
		if(!Number.isSafeInteger(oracleUnitsPerBch))
		{
			throw(new Error(`Expected oracleUnitsPerBch to be a safe javascript integer but got ${oracleUnitsPerBch}.`));
		}

		// Validate as positive
		if(!(oracleUnitsPerBch > 0))
		{
			throw(new Error(`Expected oracleUnitsPerBch to be positive but got ${oracleUnitsPerBch}.`));
		}
	}
}
