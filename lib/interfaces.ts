/**
 * @typedef  {Object} SignedOracleMessage
 *
 * @property {Uint8Array} message    Encoded oracle message
 * @property {Uint8Array} publicKey  Oracle's public key
 * @property {Uint8Array} signature  Oracle's signature over the passed message
 */
export interface SignedOracleMessage
{
	message: Uint8Array;
	publicKey: Uint8Array;
	signature: Uint8Array;
}

/**
 * @typedef  {Object} OracleMessage
 *
 * @property {number}       messageTimestamp     Timestamp of when the data was recorded by the oracle.
 * @property {number}       messageSequence      Sequence number for a message in relation to all messages from the same oracle.
 * @property {number}       dataSequenceOrType   Sequence number for a data point in relation to other data messages from the same oracle if positive, or metadata type indication if negative.
 * @property {Uint8Array}   dataContent          Binary content of the oracle message.
 */
export interface OracleMessage
{
	messageTimestamp: number;
	messageSequence: number;
	dataSequenceOrType: number;
	dataContent: Uint8Array;
}

/**
 * @typedef  {Object} DataMessage
 *
 * @property {number}       messageTimestamp   Timestamp when data was recorded by the oracle.
 * @property {number}       messageSequence    Sequence number for a message in relation to all messages from the same oracle.
 * @property {number}       dataSequence       Sequence number for a data point in relation to other data messages from the same oracle.
 * @property {Uint8Array}   dataContent        Binary content of the data message.
 */
export interface DataMessage
{
	messageTimestamp: number;
	messageSequence: number;
	dataSequence: number;
	dataContent: Uint8Array;
}

/**
 * @typedef  {Object} MetadataMessage
 *
 * @property {number}   messageTimestamp     Timestamp when data was recorded by the oracle.
 * @property {number}   messageSequence      Sequence number for a message in relation to all messages from the same oracle.
 * @property {number}   metadataType         Type for the metadata value provided in the metadataString, as a negative number.
 * @property {string}   metadataContent      Information about the signing oracle, as an unicode (UTF-8) string.
 */
export interface MetadataMessage
{
	messageTimestamp: number;
	messageSequence: number;
	metadataType: number;
	metadataContent: string;
}

/**
 * @typedef  {Object} PriceMessage
 *
 * @property {number}   messageTimestamp   Timestamp of when data was recorded by the oracle.
 * @property {number}   messageSequence    Sequence number for a message in relation to all messages from the same oracle.
 * @property {number}   priceSequence      Sequence number for a price in relation to other price messages by the same oracle.
 * @property {number}   priceValue         Price of an asset measured in units per Bitcoin Cash.
 */
export interface PriceMessage
{
	messageTimestamp: number;
	messageSequence: number;
	priceSequence: number;
	priceValue: number;
}

/**
 * Tuple of a topic and message content for distribution over ZeroMQ
 * @typedef  {[ Uint8Array, Uint8Array ]} ZeroMQMessage
 */
export type ZeroMQMessage = [Uint8Array, Uint8Array];

/**
 * Callback function for incoming packets
 */
export type IncomingPacketCallback = (topic: string, content: any) => void;

/**
 * Callback function for incoming requests
 */
export type IncomingRequestCallback = (content: any) => void;
