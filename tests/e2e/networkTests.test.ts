/* eslint-disable max-nested-callbacks */
import { jest } from '@jest/globals';

// Import library for managing bitcoin cash cryptography.
import { binToHex, hexToBin } from '@bitauth/libauth';

// Import the Price Oracle library.
import { OracleData, OracleNetwork, OracleProtocol } from '../../lib';

// Run normal behavior test cases.
jest.setTimeout(10000);
describe('normalTests', () =>
{
	let TOPIC_PRICE_MESSAGE: string;

	// Declare example oracle data (message and signature).
	const example =
	{
		// The example message binary result.
		message: hexToBin('09010000bd4b0900000000000000000002b504fcbc773b600a24c1c6186dc4d80cd9a2e7159958da0400050000009739cf5d'),

		// The example message signature using the testing keys.
		signature: hexToBin('71985d126e8704425368bfa04678157902b8869274a3fe4dbd87bb4cc40026779b9116ab731f33656fed03851a8fbe802f99d14608785b447aefa62c9b31b9b6'),

		// The example oracle public key.
		publicKey: hexToBin('02e059583a8bff98f72e86ec2471f5ed22467076e2ebaf007d2e9bfa8009dd45f3'),
	};

	beforeAll(async () =>
	{
		// async initialization. initialize the topic price message.
		TOPIC_PRICE_MESSAGE = binToHex(await OracleData.createPacketTopic());
	});

	// Test network functions that bind to ports

	describe('BroadcastTest', () =>
	{
		test('Broadcast a signed oracle message', async () =>
		{
			// verifies that 1 assertion is called in this test.
			expect.assertions(2);

			const handleBroadcastedMessages = jest.fn(async (topic, content) =>
			{
				await OracleNetwork.shutdownBroadcasting();

				expect(topic).toBe(TOPIC_PRICE_MESSAGE);
				expect(content).toBe(expect.objectContaining(example));
			});

			// Set up the broadcast network connection.
			await OracleNetwork.setupBroadcasting();

			// Listen for broadcasted messages and handle them with the callback handler.
			await OracleNetwork.listenToBroadcasts(handleBroadcastedMessages, OracleProtocol.ADDRESS_LOCAL);

			// Induce delay to allow connection to finalize.
			await new Promise((resolve) => setTimeout(resolve, 250));

			// Distribute a price message to connected peers.
			await OracleNetwork.broadcastPriceMessage(example.message, example.signature, example.publicKey);

			// Induce test delay to allow propagation to complete.
			await new Promise((resolve) => setTimeout(resolve, 500));

			// Shut down listening to broadcasts.
			await OracleNetwork.stopListeningToBroadcasts(OracleProtocol.ADDRESS_LOCAL);
		});

		test('Broadcast after reconnection', async () =>
		{
			// verifies that 1 assertion is called in this test.
			expect.assertions(2);

			// Set up a callback handler for broadcasted messages.
			const handleBroadcastedMessages = jest.fn((topic, content) =>
			{
				OracleNetwork
					.shutdownBroadcasting(OracleProtocol.ADDRESS_LOCAL, OracleProtocol.PORT_BROADCAST);

				expect(topic).toBe(TOPIC_PRICE_MESSAGE);
				expect(content).toBe(expect.any(Object));
			});

			// Set up the broadcast network connection.
			await OracleNetwork.setupBroadcasting(OracleProtocol.ADDRESS_LOCAL, OracleProtocol.PORT_BROADCAST);

			// Listen for broadcasted messages and handle them with the callback handler.
			await OracleNetwork.listenToBroadcasts(handleBroadcastedMessages, OracleProtocol.ADDRESS_LOCAL, OracleProtocol.PORT_BROADCAST);

			// Induce delay to allow connections to finalize.
			await new Promise((resolve) => setTimeout(resolve, 250));

			// Manually close the connection from the client side.
			OracleNetwork.broadcastSubscriber[`tcp://${OracleProtocol.ADDRESS_LOCAL}:${OracleProtocol.PORT_BROADCAST}`].close();

			// Induce delay to allow connection to sever (client side) and reconnect.
			await new Promise((resolve) => setTimeout(resolve, 500));

			// Distribute a price message to connected peers.
			await OracleNetwork.broadcastPriceMessage(example.message, example.signature, example.publicKey);

			// Induce test delay to allow propagation to complete.
			await new Promise((resolve) => setTimeout(resolve, 3000));

			// // Shut down listening to broadcasts.
			await OracleNetwork.stopListeningToBroadcasts(OracleProtocol.ADDRESS_LOCAL);
		});
	});

	describe('RelaysTest', () =>
	{
		test('Relay a signed oracle message', async () =>
		{
			// verifies that 2 assertion is called in this test.
			expect.assertions(2);

			// Set up a callback handler for relayed messages.
			const handleRelayedMessages = jest.fn(async (topic, content) =>
			{
				// Shut down listening for relay messages.
				await OracleNetwork.stopListeningToRelays();

				// shutdown relay
				await OracleNetwork.shutdownRelay(OracleProtocol.ADDRESS_LOCAL);

				expect(topic).toEqual(TOPIC_PRICE_MESSAGE);
				expect(content).toEqual(example);
			});

			// Listen for relayed messages and handle them with the callback handler.
			OracleNetwork.listenToRelays(handleRelayedMessages);

			// Set up a connection with a relay peer.
			await OracleNetwork.setupRelay(OracleProtocol.ADDRESS_LOCAL);

			// Induce delay to allow connection to finalize.
			await new Promise((resolve) => setTimeout(resolve, 250));

			// Distribute a price message to connected peers.
			await OracleNetwork.relayPriceMessage(example.message, example.signature, example.publicKey);

			// Induce test delay to allow propagation to complete.
			await new Promise((resolve) => setTimeout(resolve, 100));
		});

		test('Relay after reconnection', async () =>
		{
			// verifies that 3 assertion is called in this test.
			expect.assertions(3);

			// Set up a callback handler for relayed messages.
			const handleRelayedMessages = jest.fn(async (topic, content) =>
			{
				// Shut down listening for relay messages.
				await OracleNetwork.stopListeningToRelays();

				// shutdown relay
				await OracleNetwork.shutdownRelay(OracleProtocol.ADDRESS_LOCAL);

				expect(topic).toBe(TOPIC_PRICE_MESSAGE);
				expect(content).toBe(expect.any(Object));
			});

			// Listen for relayed messages and handle them with the callback handler.
			OracleNetwork.listenToRelays(handleRelayedMessages);

			// Set up a connection with a relay peer.
			await OracleNetwork.setupRelay(OracleProtocol.ADDRESS_LOCAL);

			// Induce delay to allow connection to finalize.
			await new Promise((resolve) => setTimeout(resolve, 250));

			// Make sure relay subscriber is active
			expect(OracleNetwork.relaySubscriber).toBeDefined();

			// Manually close the connection from the client side.
			OracleNetwork.relaySubscriber?.close();

			// Induce delay to allow connection to sever (client side) and reconnect.
			await new Promise((resolve) => setTimeout(resolve, 500));

			// Distribute a price message to connected peers.
			await OracleNetwork.relayPriceMessage(example.message, example.signature, example.publicKey);

			// Induce test delay to allow propagation to complete.
			await new Promise((resolve) => setTimeout(resolve, 500));
		});
	});

	describe('Request Reply', () =>
	{
		afterEach(async () =>
		{
			// Shut down listening for relay messages.
			await OracleNetwork.stopListeningToRequests();
		});
		test('Request a ping response', async () =>
		{
			// verifies that 1 assertion is called in this test.
			expect.assertions(1);

			// Set up a callback handler for broadcasted messages.
			const handleRequest = async function() : Promise<void>
			{
				await OracleNetwork.reply('pong!');
			};

			// Listen for requests and handle them with the callback handler.
			OracleNetwork.listenToRequests(handleRequest);

			// Induce delay to allow connection to finalize.
			await new Promise((resolve) => setTimeout(resolve, 250));

			// Distribute a price message to connected peers.
			const response = await OracleNetwork.request('ping!', OracleProtocol.ADDRESS_LOCAL);

			// Verify that the broadcasted data matches our expectations.
			// 'Requested ping should receive pong as response.'
			expect(response).toBe('pong!');
		});

		test('Request after reconnection', async () =>
		{
			// verifies that 2 assertion is called in this test.
			expect.assertions(2);

			// Set up a callback handler for broadcasted messages.
			const handleRequest = async function() : Promise<void>
			{
				await OracleNetwork.reply('pong!');
			};

			// Listen for requests and handle them with the callback handler.
			OracleNetwork.listenToRequests(handleRequest);

			// Induce delay to allow connection to finalize.
			await new Promise((resolve) => setTimeout(resolve, 250));

			// Make sure replySocket is active
			expect(OracleNetwork.replySocket).toBeDefined();

			// Manually close the connection from the client side.
			OracleNetwork.replySocket?.close();

			// Induce delay to allow connection to sever (client side) and reconnect.
			await new Promise((resolve) => setTimeout(resolve, 500));

			// Distribute a price message to connected peers.
			const response = await OracleNetwork.request('ping!', OracleProtocol.ADDRESS_LOCAL);

			// Verify that the broadcasted data matches our expectations.
			// Requested ping should receive pong as response, even after reconnection.
			expect(response).toBe('pong!');
		});
	});
});

describe('failureTests', () =>
{
	test.todo('Run test cases that verify failure behaviors');
});
