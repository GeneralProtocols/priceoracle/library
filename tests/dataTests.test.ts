/* eslint-disable max-nested-callbacks */
// Import library for managing bitcoin cash cryptography.
import { secp256k1, decodePrivateKeyWif, hexToBin, flattenBinArray, WalletImportFormatType } from '@bitauth/libauth';

// Import the Price Oracle library.
import { OracleData } from '../lib';

// Wrap the testing in an async function to enable async/await syntax.
describe('data tests', () =>
{
	// Load the private key WIF.
	const oraclePrivateKeyWIF = 'KxWg2mMcFFTUJj3Gq7HvHAfiMxeHfq9UqrpT6q9CvD66D4XLnTLf';

	// Derive the public key.
	let oraclePublicKey: Uint8Array;

	// Declare example oracle data (message and signature).
	const example =
	{
		// The example messages properties.
		data:
		{
			// The timestamp below is for 2019-11-15 23:49:43 UTC.
			messageTimestamp: 1573861783,
			messageSequence:  5,
			priceSequence:    4,
			priceValue:       265,
		},

		// The example message binary result.
		message: hexToBin('9739cf5d050000000400000009010000'),

		// The example message signature using the testing keys.
		signature: hexToBin('2ed4e57f62576d2255ae2457d524ddace7e4640d676dc252415127b526335fb8bf20491f3c85fbb9d449713c810cbece61e5768a9def81170dbacd0265848ce1'),

		// The example message signature using the testing keys, with the first 4 bytes set to zero.
		invalid_signature: hexToBin('00000000eedfcf585eb82dda78306dd65e7fddad2f93a4da9a2c6fb3759bb7a493c45f9382eaa23374b6ea4a01b63590e67f8d649d5abcc5af47d982ac31887b'),

		// The example package for distribution:
		priceMessagePackage: hexToBin('6a0471808079109739cf5d0500000004000000090100002102e059583a8bff98f72e86ec2471f5ed22467076e2ebaf007d2e9bfa8009dd45f3402ed4e57f62576d2255ae2457d524ddace7e4640d676dc252415127b526335fb8bf20491f3c85fbb9d449713c810cbece61e5768a9def81170dbacd0265848ce1'),

		// An invalid price message package for distribution:
		invalidPackage: hexToBin('6a047180807901FF0011223344556677889900AABBCCDDEEFF'),
	};

	beforeAll(async () =>
	{
		// Decode the private key WIF into a private key.
		const decodeResult = await decodePrivateKeyWif(oraclePrivateKeyWIF) as { privateKey: Uint8Array; type: WalletImportFormatType };

		// Extract the result
		const oraclePrivateKey = decodeResult.privateKey;

		// Derive the public key.
		const derivedPublicKeyOrError = secp256k1.derivePublicKeyCompressed(oraclePrivateKey);

		if(typeof derivedPublicKeyOrError === 'string')
		{
			throw(new Error(`Could not derive public key: ${derivedPublicKeyOrError}`));
		}

		oraclePublicKey = derivedPublicKeyOrError;
	});

	// Run normal behavior test cases.
	describe('normal tests', () =>
	{
		// Test top-level non-stubbed library functions in parallel with the current use case.
		test('Create oracle message', async () =>
		{
			// Craft a price message from the example data.
			const craftedMessage = await OracleData.createPriceMessage(example.data.messageTimestamp, example.data.messageSequence,
				example.data.priceSequence, example.data.priceValue);

			// Verify that created price message matches expectations.
			// 'Crafted message should not change when the message properties is not changed.'
			expect(craftedMessage).toEqual(example.message);
		});

		test('Parse oracle message', async () =>
		{
		// Parse the example price message.
			const parsedMessage = await OracleData.parsePriceMessage(example.message);

			// Verify that the parsed message matches the expectations.
			// Parsed message properties should not change when the message is not changed.
			expect(parsedMessage).toEqual(example.data);
		});
		test('Sign oracle message', async () =>
		{
			// Sign the example message.
			const signature = await OracleData.signMessage(example.message, oraclePrivateKeyWIF);

			// Verify that the signature matches the expectations.
			// Message signature should not change when message and signature is not changed.
			expect(signature).toEqual(example.signature);
		});

		test('Verify oracle message', async () =>
		{
			// Process the verification of the example message and signature.
			const signatureStatus = await OracleData.verifyMessageSignature(example.message, example.signature, oraclePublicKey);

			// Verify that the signature verification results in true.
			// Message verification should succeed when the message and signature are valid.
			expect(signatureStatus).toBe(true);
		});
		test('Package oracle message', async ()	=>
		{
			// Package the example message, signature and data for distribution.
			const distributionParts = await OracleData.packOracleMessage(example.message, example.signature, oraclePublicKey);

			// Concatenate the topic and message before comparison.
			const distributionPackage = flattenBinArray(distributionParts);

			// Verify binary package matches out expectations.
			// Package should not change when message, signature and public key are unchanged.
			expect(distributionPackage).toEqual(example.priceMessagePackage);
		});
		test('Unpack oracle message', async ()	=>
		{
			// Unpack the example package to get the message, signature and public key.
			const packageData = await OracleData.unpackOracleMessage(example.priceMessagePackage);

			// Assemble the expected data parts into an object to compare with.
			const expectedData = { message: example.message, signature: example.signature, publicKey: oraclePublicKey };

			// Verify that the unpacked data matches our expectations.
			// Unpacked data should not change when the package has not been changed.
			expect(packageData).toEqual(expectedData);
		});
	});

	// Run test cases that verify failure behaviors.
	describe('failure tests', () =>
	{
		// Test top-level non-stubbed library functions in parallel with the current use case.
		test('Fail on missing signature.', async () =>
		{
			// Process the verification of the example message and an empty signature.
			const signatureStatus = await OracleData.verifyMessageSignature(example.message, null as any, oraclePublicKey);

			// Verify that the signature verification results in false.
			// Message verification should fail when the signature is empty.
			expect(signatureStatus).toBe(false);
		});
		// Test top-level non-stubbed library functions in parallel with the current use case.
		test('Fail on empty signature.', async () =>
		{
			// Process the verification of the example message and an empty signature.
			const signatureStatus = await OracleData.verifyMessageSignature(example.message, new Uint8Array(0), oraclePublicKey);

			// Verify that the signature verification results in false.
			// Message verification should fail when the signature is empty.
			expect(signatureStatus).toBe(false);
		});
		test('Fail on invalid signature.', async () =>
		{
			// Process the verification of the example message and an empty signature.
			const signatureStatus = await OracleData.verifyMessageSignature(example.message, example.invalid_signature, oraclePublicKey);

			// Verify that the signature verification results in false.
			// Message verification should fail when the signature is invalid.
			expect(signatureStatus).toBe(false);
		});
		test('Fail on too short message', async () =>
		{
			// Try to parse an intentionally insufficiently long message.
			// Message parsing should throw error when message is too short.
			await expect(() => OracleData.parsePriceMessage(hexToBin('01'))).rejects.toThrow('Failed to parse oracle message, provided message size (1) is not large enough.');
		});
		test('Fail on too long message', async () =>
		{
			// Try to parse an intentionally excessively long message.
			// Message parsing should throw error when message is too long.
			await expect(() => OracleData.parsePriceMessage(hexToBin('0101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101010101')))
				.rejects.toThrow('Failed to parse oracle message, provided message size (50) does not match the expected size of 16 bytes.');
		});
		test('Fail on incorrect topic', async () =>
		{
			// Unpack the example package to get the message, signature and public key.
			// Unpacking price message should fail when using incorrect topic.
			await expect(() => OracleData.unpackOracleMessage(example.invalidPackage)).rejects.toThrow('Failed to unpack message: invalid structure.');
		});
	});
});
