// Import testing framework and dependencies.
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { jest } from '@jest/globals';

// NOTE: These are not pure unit tests. They test all top level methods of the OracleUnits class
//       which exercises all internal methods. It could be improved by mocking internal calls
//       and directly testing internal methods.

// Import the OracleUnits for testing
import { OracleUnits, Satoshis } from '../lib/units';

// Create some reusable valid values
const VALID_ORACLE_UNIT_AMOUNT = 1234;
const VALID_ORACLE_UNITS_PER_COMMON_UNIT = 1111;
const VALID_PRICE_IN_ORACLE_UNITS_PER_BCH = 321;
const VALID_SATS_AMOUNT = 2222;
const VALID_BCH_AMOUNT = 3.12345678;
const VALID_ORACLE_UNITS_INSTANCE = new OracleUnits(VALID_ORACLE_UNIT_AMOUNT, VALID_ORACLE_UNITS_PER_COMMON_UNIT);

// Create some reusable problem values
const NOT_FINITE_NUMBERS: Array<any> = [
	// not a number
	undefined,
	null,

	// special floating points
	NaN,
	Number.POSITIVE_INFINITY,
];
const NOT_SAFE_INTEGERS: Array<any> = [
	...NOT_FINITE_NUMBERS,

	// not an integer
	0.1,
	1.1,

	// unsafe integer
	Number.MAX_SAFE_INTEGER + 1000000000,
];
const NOT_POSITIVE_SAFE_INTEGERS: Array<any> = [
	...NOT_SAFE_INTEGERS,

	// integer, but negative
	-100,
];

describe('OracleUnits constructor', () =>
{
	test('given valid inputs, constructor stores the original input values', async () =>
	{
		// Confirm the stored value is the same as the original
		expect(VALID_ORACLE_UNITS_INSTANCE.oracleUnits).toEqual(VALID_ORACLE_UNIT_AMOUNT);
		expect(VALID_ORACLE_UNITS_INSTANCE.oracleUnitsPerCommonUnit).toEqual(VALID_ORACLE_UNITS_PER_COMMON_UNIT);
	});

	test('given invalid oracle unit input amount (not safe integer), constructor throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badOracleUnits of NOT_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => new OracleUnits(badOracleUnits, VALID_ORACLE_UNITS_PER_COMMON_UNIT),
			).toThrow(`${badOracleUnits}`);
		}
	});

	test('given invalid oracle units per common unit input (not positive, safe integer), constructor throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badScaler of NOT_POSITIVE_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => new OracleUnits(VALID_ORACLE_UNIT_AMOUNT, badScaler),
			).toThrow(`${badScaler}`);
		}
	});
});

describe('OracleUnits fromCommonUnits', () =>
{
	test('given valid inputs, fromCommonUnits stores expected base values', async () =>
	{
		// Create some values that should work
		type TestCase = {
			commonUnits: number;
			oracleUnitsPerCommonUnit: number;
			expectedOracleUnits: number;
		};

		const testCases: Array<TestCase> = [
			{
				commonUnits: 1000,
				oracleUnitsPerCommonUnit: 1,
				expectedOracleUnits: 1000,
			},
			{
				commonUnits: 1000,
				oracleUnitsPerCommonUnit: 10,
				expectedOracleUnits: 10000,
			},
			{
				commonUnits: 10.55,
				oracleUnitsPerCommonUnit: 10,
				expectedOracleUnits: 106,
			},
		];

		for(const testCase of testCases)
		{
			// Create the instance
			const oracleUnits = OracleUnits.fromCommonUnits(testCase.commonUnits, testCase.oracleUnitsPerCommonUnit);

			// Confirm the base values are as expected
			expect(oracleUnits.oracleUnits).toEqual(testCase.expectedOracleUnits);
			expect(oracleUnits.oracleUnitsPerCommonUnit).toEqual(testCase.oracleUnitsPerCommonUnit);
		}
	});

	test('given invalid common unit input (not finite number), fromCommonUnits throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badCommonInputUnits of NOT_FINITE_NUMBERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => OracleUnits.fromCommonUnits(badCommonInputUnits, VALID_ORACLE_UNITS_PER_COMMON_UNIT),
			).toThrow(`${badCommonInputUnits}`);
		}
	});
});

describe('OracleUnits fromSatoshis', () =>
{
	test('given valid inputs, fromSats stores expected base values', async () =>
	{
		// Create some values that should work
		type TestCase = {
			satoshis: number;
			priceInOracleUnitsPerBch: number;
			oracleUnitsPerCommonUnit: number;
			expectedOracleUnits: number;
		};

		const testCases: Array<TestCase> = [
			{
				// 1 BCH, 10000 INR/BCH, scaling 1
				// --> (1e8 Sats / 1e8 Sats/Bch) * (10k OracleINR/BCH)
				// --> 10000 OracleINR
				satoshis: 100_000_000,
				priceInOracleUnitsPerBch: 10_000,
				oracleUnitsPerCommonUnit: 1,
				expectedOracleUnits: 10000,
			},
			{
				// 0.1 BCH, 10758 INR/BCH, scaling 1
				// --> (1e7 Sats / 1e8 Sats/Bch) * (10758 OracleINR/BCH)
				// --> 1076 OracleINR
				satoshis: 10_000_000,
				priceInOracleUnitsPerBch: 10758,
				oracleUnitsPerCommonUnit: 1,
				expectedOracleUnits: 1076,
			},
			{
				// 0.1 BCH, 133.33 USD/BCH, scaling 100
				// --> (1e7 Sats / 1e8 Sats/Bch) * (133_33 OracleUSD/BCH)
				// --> 1333 OracleUSD
				satoshis: 10_000_000,
				priceInOracleUnitsPerBch: 13333,
				oracleUnitsPerCommonUnit: 100,
				expectedOracleUnits: 1333,
			},
		];

		for(const testCase of testCases)
		{
			// Create the instance
			const oracleUnits = OracleUnits.fromSatoshis(testCase.satoshis, testCase.oracleUnitsPerCommonUnit, testCase.priceInOracleUnitsPerBch);

			// Confirm the base values are as expected
			expect(oracleUnits.oracleUnits).toEqual(testCase.expectedOracleUnits);
			expect(oracleUnits.oracleUnitsPerCommonUnit).toEqual(testCase.oracleUnitsPerCommonUnit);
		}
	});

	test('given invalid satoshis input (not safe integer), fromSats throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badSatoshis of NOT_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => OracleUnits.fromSatoshis(badSatoshis, VALID_ORACLE_UNITS_PER_COMMON_UNIT, VALID_PRICE_IN_ORACLE_UNITS_PER_BCH),
			).toThrow(`${badSatoshis}`);
		}
	});

	test('given invalid price input (not positive, safe integer), fromSats throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badPrice of NOT_POSITIVE_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => OracleUnits.fromSatoshis(VALID_SATS_AMOUNT, VALID_ORACLE_UNITS_PER_COMMON_UNIT, badPrice),
			).toThrow(`${badPrice}`);
		}
	});
});

describe('OracleUnits fromBch', () =>
{
	test('given valid inputs, fromBch stores expected base values', async () =>
	{
		// Create some values that should work
		type TestCase = {
			bch: number;
			priceInOracleUnitsPerBch: number;
			oracleUnitsPerCommonUnit: number;
			expectedOracleUnits: number;
		};

		const testCases: Array<TestCase> = [
			{
				// 1 BCH, 10000 INR/BCH, scaling 1
				// --> 1 BCH * (10k OracleINR/BCH)
				// --> 10000 OracleINR
				bch: 1,
				priceInOracleUnitsPerBch: 10_000,
				oracleUnitsPerCommonUnit: 1,
				expectedOracleUnits: 10000,
			},
			{
				// 0.1 BCH, 10758 INR/BCH, scaling 1
				// --> 0.1 BCH * (10758 OracleINR/BCH)
				// --> 1076 OracleINR
				bch: 0.1,
				priceInOracleUnitsPerBch: 10758,
				oracleUnitsPerCommonUnit: 1,
				expectedOracleUnits: 1076,
			},
			{
				// 0.1 BCH, 133.33 USD/BCH, scaling 100
				// --> 0.1 BCH * (133_33 OracleUSD/BCH)
				// --> 1333 OracleUSD
				bch: 0.1,
				priceInOracleUnitsPerBch: 13333,
				oracleUnitsPerCommonUnit: 100,
				expectedOracleUnits: 1333,
			},
		];

		for(const testCase of testCases)
		{
			// Create the instance
			const oracleUnits = OracleUnits.fromBch(testCase.bch, testCase.oracleUnitsPerCommonUnit, testCase.priceInOracleUnitsPerBch);

			// Confirm the base values are as expected
			expect(oracleUnits.oracleUnits).toEqual(testCase.expectedOracleUnits);
			expect(oracleUnits.oracleUnitsPerCommonUnit).toEqual(testCase.oracleUnitsPerCommonUnit);
		}
	});

	test('given invalid bch input (not finite, <8 digit number), fromBch throws', async () =>
	{
		// Add a special 9-digit invalid case to the shared invalid input set
		const badBchValues: Array<any> = [
			0.123456789,
			...NOT_FINITE_NUMBERS,
		];

		// Confirm that it throws for each of the not finite problem values
		for(const badBch of badBchValues)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => OracleUnits.fromBch(badBch, VALID_ORACLE_UNITS_PER_COMMON_UNIT, VALID_PRICE_IN_ORACLE_UNITS_PER_BCH),
			).toThrow(`${badBch}`);
		}
	});

	test('given invalid price input (not positive, safe integer), fromBch throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badPrice of NOT_POSITIVE_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => OracleUnits.fromBch(VALID_BCH_AMOUNT, VALID_ORACLE_UNITS_PER_COMMON_UNIT, badPrice),
			).toThrow(`${badPrice}`);
		}
	});
});

describe('OracleUnits toOracleUnits', () =>
{
	test('returns the base oracle units value', async () =>
	{
		expect(VALID_ORACLE_UNITS_INSTANCE.toOracleUnits()).toEqual(VALID_ORACLE_UNIT_AMOUNT);
	});
});

describe('OracleUnits toCommonUnits', () =>
{
	test('returns a full precision conversion to common units', async () =>
	{
		// Create some values that should work
		type TestCase = {
			oracleUnits: OracleUnits;
			expectedCommonUnits: number;
		};

		const testCases: Array<TestCase> = [
			{
				oracleUnits: new OracleUnits(1000, 1),
				expectedCommonUnits: 1000,
			},
			{
				oracleUnits: new OracleUnits(1000, 10),
				expectedCommonUnits: 100,
			},
			{
				// The result is 0.090009001, but we put the raw division to indicate this is just a normal floating point
				oracleUnits: new OracleUnits(100, 1111),
				expectedCommonUnits: 100 / 1111,
			},
		];

		for(const testCase of testCases)
		{
			// Confirm the base values are as expected
			expect(testCase.oracleUnits.toCommonUnits()).toEqual(testCase.expectedCommonUnits);
		}
	});
});

describe('OracleUnits toCommonUnitsFormatted', () =>
{
	test('given a numeric rounding directive, returns a string conversion to common units rounded as indicated', async () =>
	{
		// Create some values that should work
		type TestCase = {
			oracleUnits: OracleUnits;
			roundToDigits: number;
			expectedCommonUnitsFormatted: string;
		};

		const testCases: Array<TestCase> = [
			{
				oracleUnits: new OracleUnits(1000, 1),
				roundToDigits: 0,
				expectedCommonUnitsFormatted: '1000',
			},
			{
				oracleUnits: new OracleUnits(1000, 1),
				roundToDigits: 1,
				expectedCommonUnitsFormatted: '1000.0',
			},
			{
				oracleUnits: new OracleUnits(1111, 100),
				roundToDigits: 2,
				expectedCommonUnitsFormatted: '11.11',
			},
			{
				// The result is 0.090009001, but we put the raw division to indicate this is just a normal floating point
				oracleUnits: new OracleUnits(100, 1111),
				roundToDigits: 4,
				expectedCommonUnitsFormatted: '0.0900',
			},
		];

		for(const testCase of testCases)
		{
			// Confirm the base values are as expected
			expect(testCase.oracleUnits.toCommonUnitsFormatted(testCase.roundToDigits)).toEqual(testCase.expectedCommonUnitsFormatted);
		}
	});

	test('given a round-to-display directive, returns a conversion to common units rounded by the internal display digits heuristic', async () =>
	{
		// Create some values that should work
		type TestCase = {
			oracleUnits: OracleUnits;
			expectedCommonUnitsFormatted: string;
		};

		const testCases: Array<TestCase> = [
			{
				// conversion 1 --> internal heuristic of (1-1)=0 digits
				oracleUnits: new OracleUnits(1000, 1),
				expectedCommonUnitsFormatted: '1000',
			},
			{
				// conversion 100 --> internal heuristic of (3-1)=2 digits
				oracleUnits: new OracleUnits(1111, 100),
				expectedCommonUnitsFormatted: '11.11',
			},
			{
				// conversion 9999 --> internal heuristic of (4-1)=3 digits
				// The result is 0.090009001, but we put the raw division to indicate this is just a normal floating point
				oracleUnits: new OracleUnits(100, 1111),
				expectedCommonUnitsFormatted: '0.090',
			},
		];

		for(const testCase of testCases)
		{
			// Confirm the base values are as expected
			expect(testCase.oracleUnits.toCommonUnitsFormatted('round_to_display')).toEqual(testCase.expectedCommonUnitsFormatted);
		}
	});
});

describe('OracleUnits toSatoshis', () =>
{
	test('given a valid price input, returns the base oracle units converted to satoshis', async () =>
	{
		// Hand calculate an expected result
		// satoshis = round((oracle unit amount / price oracle units per bch) * satoshis per bch)
		//          = round((123456 / 78) * 1e8)
		//          = round(158276923076.9)
		//          = 158276923077
		const oracleUnitAmount = 123456;
		const priceOracleUnitsPerBch = 78;
		const expectedSats = 158276923077;

		const oracleUnits = new OracleUnits(oracleUnitAmount, VALID_ORACLE_UNITS_PER_COMMON_UNIT);

		// Confirm the base values are as expected
		expect(oracleUnits.toSatoshis(priceOracleUnitsPerBch)).toEqual(expectedSats);
	});

	test('given invalid price input (not positive, safe integer), toSats throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badPrice of NOT_POSITIVE_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => VALID_ORACLE_UNITS_INSTANCE.toSatoshis(badPrice),
			).toThrow(`${badPrice}`);
		}
	});
});

describe('OracleUnits toBch', () =>
{
	test('given a valid price input, returns the base oracle units converted to satoshis', async () =>
	{
		// Hand calculate an expected result
		// bch = roundTo8Digits(oracle unit amount / price oracle units per bch)
		//      = roundTo8Digits(123456 / 78)
		//      = roundTo8Digits(1582.769230769)
		//      = 1582.76923077
		const oracleUnitAmount = 123456;
		const priceOracleUnitsPerBch = 78;
		const expectedBch = 1582.76923077;

		const oracleUnits = new OracleUnits(oracleUnitAmount, VALID_ORACLE_UNITS_PER_COMMON_UNIT);

		// Confirm the base values are as expected
		expect(oracleUnits.toBch(priceOracleUnitsPerBch)).toEqual(expectedBch);
	});

	test('given invalid price input (not positive, safe integer), toBch throws', async () =>
	{
		// Confirm that it throws for each problem value
		for(const badPrice of NOT_POSITIVE_SAFE_INTEGERS)
		{
			// Confirm that the error includes the problem value
			await expect(
				() => VALID_ORACLE_UNITS_INSTANCE.toBch(badPrice),
			).toThrow(`${badPrice}`);
		}
	});
});

describe('OracleUnits getDecimalPlacesForCommonUnitDisplay', () =>
{
	test('returns a number of digits according to a heuristic that works well in practice', async () =>
	{
		// Do several confirmations of the simple heuristic which is (length of the scaling factor - 1).
		// E.g. for the USD/BCH oracle, the scaling factor is 100. Then the heuristic is len('100')-1 = 2 digits.

		// Test case format
		type TestCase = {
			oracleUnitsPerCommonUnit: number;
			expectedDecimalPlaces: number;
		};

		// Several test cases that demonstrate the heuristic
		const testCases: Array<TestCase> =
		[
			{ oracleUnitsPerCommonUnit: 1, expectedDecimalPlaces: 0 },
			{ oracleUnitsPerCommonUnit: 5, expectedDecimalPlaces: 0 },
			{ oracleUnitsPerCommonUnit: 10, expectedDecimalPlaces: 1 },
			{ oracleUnitsPerCommonUnit: 1000, expectedDecimalPlaces: 3 },
		];

		// Confirm that we get the correct result on each input
		for(const testCase of testCases)
		{
			// Create the oracle units instance
			// Note that oracle unit amount is ignored for these purposes, so we just use some valid amount.
			const oracleUnits = new OracleUnits(VALID_ORACLE_UNIT_AMOUNT, testCase.oracleUnitsPerCommonUnit);

			// Confirm that we get the expected decimal place count
			expect(oracleUnits.getDecimalPlacesForCommonUnitDisplay()).toEqual(testCase.expectedDecimalPlaces);
		}
	});
});

describe('Satoshis toBch', () =>
{
	test('given valid inputs, fromBch stores expected base values', async () =>
	{
		// Create some values that should work
		type TestCase = {
			bch: number;
			expectedSatsBigInt: bigint;
			expectedSatsNumber: number;
		};

		const testCases: Array<TestCase> = [
			{
				// Regression test for former bug that allowed unhandled floating point artifacts (x.000000001)
				// Specifically 115/13455 -> 0.008547008547008548 -> formatted to 8 digits -> 0.00854701
				// results in 854701.0000000001 which was previously not rounded, causing a validation error
				bch: 0.00854701,
				expectedSatsBigInt: 854701n,
				expectedSatsNumber: 854701,
			},
		];

		for(const testCase of testCases)
		{
			// Create the instance
			const satoshis = Satoshis.fromBch(testCase.bch);

			// Confirm the base values are as expected
			expect(satoshis.toSats()).toEqual(testCase.expectedSatsBigInt);
			expect(satoshis.toSatoshis()).toEqual(testCase.expectedSatsNumber);
		}
	});

	test('given valid inputs, toBch outputs expected values', async () =>
	{
		// Create some values that should work
		type TestCase = {
			sats: number | bigint;
			bch: number;
		};

		const testCases: Array<TestCase> = [
			// Valid number.
			{
				sats: 854701,
				bch: 0.00854701,
			},
			// Valid bigint.
			{
				sats: 854701n,
				bch: 0.00854701,
			},
		];

		for(const testCase of testCases)
		{
			// Create the instance
			const satoshis = Satoshis.fromSats(testCase.sats);

			// Confirm the base values are as expected
			expect(satoshis.toBch()).toEqual(testCase.bch);
		}
	});

	test('given invalid inputs, fromSats throw an error', async () =>
	{
		const testCases: Array<number> = [
			// Non-integer value.
			12345.5,
		];

		for(const invalidNumber of testCases)
		{
			// Expect the invalid test case to throw an error.
			expect(() => Satoshis.fromSats(invalidNumber)).toThrow();
		}
	});
});
