// Import package.json to reduce name repetition.
// eslint-disable-next-line import/extensions
import pkg from './package.json';
import dts from 'rollup-plugin-dts';

export default
[
	// Build main module in both ES and CJS
	{
		// Specify entry point for building
		input: 'build/lib/index.js',

		// Omit all dependencies and peer dependencies from the final bundle
		external: [
			...Object.keys(pkg.dependencies || {}),
			...Object.keys(pkg.peerDependencies || {}),
		],

		// Produce outputs for ES and CJS
		output: [
			{ file: pkg.exports.node.import, format: 'es' },
			{ file: pkg.exports.node.require, format: 'cjs' },
		],
	},
	// Build a browser module in both ES and CJS
	// NOTE: The browser build omits OracleNetwork due to the ZeroMQ dependency.
	{
		// Specify entry point for building
		input: 'build/lib/browser.js',

		// Omit all dependencies and peer dependencies from the final bundle
		external: [
			...Object.keys(pkg.dependencies || {}),
			...Object.keys(pkg.peerDependencies || {}),
		],

		// Produce outputs for ES and CJS
		output: [
			{ file: pkg.exports.browser.import, format: 'es' },
			{ file: pkg.exports.browser.require, format: 'cjs' },
		],
	},
	// Build type definitions for the library
	{
		input: 'build/lib/index.d.ts',
		output: [{ file: pkg.typings, format: 'es' }],
		plugins: [ dts() ],
	},
];
