export default  {
	transform: {},
	// Allow Jest to natively transpile TS files
	testEnvironment: 'node',
	transformIgnorePatterns: [ '<rootDir>/node_modules/' ],

	// Configure reporters
	reporters: [ 'default', 'jest-junit' ],
	testResultsProcessor: 'jest-junit',

	// Only include lib folder when checking code coverage
	collectCoverageFrom: [
		'lib/**/*.ts',
	],
	coverageReporters: [ 'text', 'lcov' ],

	// Configure Unit Tests and E2E tests
	projects: [
		{
			displayName: 'unit',
			testMatch: [ '<rootDir>/build/tests/*.(spec|test).js' ],
		},
		{
			displayName: 'e2e',
			testMatch: [ '<rootDir>/build/tests/e2e/**/*.(spec|test).js' ],
			maxConcurrency: 1,
		},
	],
};
