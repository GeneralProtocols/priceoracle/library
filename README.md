# PriceOracle Libraries

Library for creating, parsing, signing, verifying and transferring oracle price messages according to the [PriceOracle Specification](https://gitlab.com/GeneralProtocols/priceoracle/specification).


## Best Practices For Price Oracle Consumers

Smart contracts can involve amounts of money ranging from tiny test amounts to large fortunes, and lock times from minutes to years.
Below are some minimal considerations before you make a contract that depends on a price oracle.

- As a starting point, never make large or long contracts with a price oracle that is in beta or that you have not done due diligence on.
- Does the price oracle commit to strictly following the [specification](https://gitlab.com/GeneralProtocols/priceoracle/specification), and do you trust the commitment?
- Does the price oracle commit to operation for the time period of my contract, and do you trust the commitment?
- Do you trust the price oracle to maintain its security and not expose its signing key?

Price oracle [metadata](https://gitlab.com/GeneralProtocols/priceoracle/specification#oracle-information-messages) answers most due diligence questions in a technical sense, but you must also consider the larger picture of how much trust you place in a price oracle's commitments.


## Installation

Install the library via NPM:

```
# npm install @generalprotocols/price-oracle
```

## Usage

### Setup

Before you can use the Price Oracle functionality in your project, you need to import it to your project:

```js
// Import the Price Oracle library.
import { OracleData, OracleNetwork, OracleProtocol } from '@generalprotocols/price-oracle';
```

### Oracle Data

The OracleData class provides utility functions for managing price messages and makes it easy to create, parse, sign and verify signed prices messages.

```js
// Create an oracle price message from parts.
const message = await OracleData.createPriceMessage(messageTimestamp, messageSequence, priceSequence, priceValue);

// Parse an oracle price message into parts.
const messageParts = await OracleData.parsePriceMessage(message);

// Sign an oracle price message.
const signature = await OracleData.signMessage(message, privateKeyWIF);

// Verify an oracle price message signature.
const validity = await OracleData.verifyMessageSignature(message, signature, publicKey);
```

### Oracle Network

The [PriceOracle Specification](https://gitlab.com/GeneralProtocols/priceoracle/specification) contains directives for how to distribute price messages in different ways.
The OracleNetwork class provides utility functions for interfacing with other oracles.

*Note: the oracles can form a network and relay data, but does not have automatic peer discovery, so you will need to manually choose what oracles to connect to.*

#### Broadcast / Push

To `broadcast` to all connected peers, you first need to set up a broadcast connection after which you can broadcast messages to any currently connected peers.

*Note: broadcast default port is `TCP 7084`.*

```js
// Set up the broadcast network connection.
await OracleNetwork.setupBroadcasting();

// Wait for peers to connect

// Distribute a price message to connected peers.
const broadcastResult = await OracleNetwork.broadcastPriceMessage(message, signature, publicKey);
```

To connect to an oracle to receive broadcasted messages:

```js
// Set up a callback handler for broadcasted messages.
const handleBroadcastedMessages = function(topic, content)
{
	// Check for the types you care about..
	if(topic === OracleProtocol.TOPIC_PING)
	{
		// Do something interesting with the message here.
	}
}

// Listen for broadcasted messages and handle them with the callback handler.
await OracleNetwork.listenToBroadcasts(handleBroadcastedMessages, oracleAddress);
```

#### Request - Reply / Pull

To send a generic request:

*Note: request-reply default port is `TCP 7083`.*

```js
// Send a generic request from the external oracle.
const response = await OracleNetwork.request('ping!', oracleAddress);
```

To respond to a request:

```js
// Set up a request handler.
const handleRequest = function(content)
{
	// Send the reply.
	OracleNetwork.reply('pong!');
}

// Listen for requests and handle them with the callback handler.
await OracleNetwork.listenToRequests(handleRequest);
```

#### Relay

Relay allows you to send data directly to specific peers by connecting to them individually. It functions similar to broadcasting and is intended to be used to setup fixed private connections.

To relay a message to another peer:

*Note: relay default port is `TCP 7085`.*

```js
// Set up a connection with a relay peer.
await OracleNetwork.setupRelay(oracleAddress);

// Forward a message to all connected relay peers.
await OracleNetwork.relay(topic, content);
```

To receive relayed message from peers:

```js
// Set up a callback handler for relayed messages.
const handleRelayedMessages = function(topic, content)
{
	// Do something interesting with the message here.
}

// Listen for relayed messages and handle them with the callback handler.
await OracleNetwork.listenToRelays(handleRelayedMessages);
```
